# Capstone project 

### AWS Infrastructure

<br>

- Amazon S3 bucket: Store the static content of the web application, basically the "dist" folder which contains the compiled and bundled version of frontend application ready for deployment. Also, the S3 bucket stores the JAR (Java Archive) file and this file is downloaded to the EC2 instances.
- EC2 instance: Host the JAR file. The web server for this application is Apache Tomcat and Spring Boot includes an embedded Tomcat server as a dependency.
- Database instance: PostgreSQL primary and replica database
- Application load balancer is used to distribute incoming network traffic across multiple EC2 instances
- Auto scaling group is used to automatically scale in (less) or scale out (more) EC2 instances based on configuration.
- Terraform: an open-source infrastructure as code tool used for managing AWS resources.
- Git is used for versioning project code which is hosted on GitHub repository.

<br>

![capstoneDiagramm](https://gitlab.com/d5911/aws-infrastructure/-/raw/main/Documentation/Capstone_Project_Infrastructure.png
)
